> **This project is deprecated and won’t receive any update, only CVE fixes!**

# Client Java Examples

These examples show typical use of the different types of kafka clients runnable against the Axual Platform.
All examples illustrate the same use-cases, making it easier to compare them.

## Project Layout

- [Native Kafka Client Examples](src/main/java/io/axual/examples/kafka)

## How to use the examples

The examples should be used against an Axual (trial) platform, but any kafka-cluster can be used.

Check the [Axual reference](https://docs.axual.io) to see how to get a trial platform running, and how to get started with these examples.

The examples expect the kafka-topics to exist and the certificate in the resources directory to be authorised to produce and consume from them. Check the [Configuration](src/main/java/io/axual/examples/util/config/Configuration.java) class for stream names.

## FAQ
### When should I use which client?


#### [Kafka Client Examples](src/main/java/io/axual/examples/kafka)

Use the native Kafka client when you don't need the switching and name-resolving logic and want to produce-to/consume-from a single cluster.

When using the native kafka client, you need to provide the fully resolved `stream/topic name` & `consumer group.id` as configuration.

If you used kafka before, this is probably the most familiar route.

#### [Spring-Kafka Examples](src/main/java/io/axual/examples/springkafka)

These examples are deprecated, to be removed when the EOL date of the client has been reached. Read more here: https://axual.com/update-on-support-of-axual-client-libraries

#### [Axual Client Examples](src/main/java/io/axual/examples/axual)

These examples are deprecated, to be removed when the EOL date of the client has been reached. Read more here: https://axual.com/update-on-support-of-axual-client-libraries


## Configuration
### How can I try the different SSL configuration options?

You can use SSL configuration interchangeably. Trust and key material can be loaded in different formats. The [KafkaSecurityUtil](src/main/java/io/axual/examples/util/KafkaSecurityUtil.java) and [AxualSSLUtil](src/main/java/io/axual/examples/util/AxualSSLUtil.java) classes illustrate every SSL configuration method.

### How can I connect to helm and how to axual-trial?
You can specify which environment you want configured by changing the implementation of Configuration within [Configuration](src/main/java/io/axual/examples/util/config/Configuration.java) as such:
```java
Configuration configuration = new TrialConfiguration();  // Option 1: for AxualTrial
Configuration configuration = new HelmConfiguration();   // Option 2: for LocalHelm
```

<sup>*</sup> **Note**: If you want to run the examples against the SaaS trial environment, the information in the README.txt file you received will need to be placed within the [TrialConfiguration](src/main/java/io/axual/examples/util/config/impl/TrialConfiguration.java) file.

### Stream vs Topic
> 2023.1 Release Update

In Self-Service UI we have changed all references to "Stream" to "Topic". Therefore we have updated the Java Helper Examples to reflect this change. There are no changes to the interface of Axual Java libraries.

## License
Axual Java Client Example is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).
