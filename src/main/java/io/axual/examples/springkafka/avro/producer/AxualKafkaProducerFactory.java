package io.axual.examples.springkafka.avro.producer;

/*-
 * #%L
 * axual-spring-kafka-avro-producer
 * %%
 * Copyright (C) 2022 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.kafka.clients.producer.Producer;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;

import java.util.Map;

import io.axual.client.proxy.axual.producer.AxualProducer;

/**
 * This factory extends the default factory provided by Spring Kafka and uses the {@link
 * AxualProducer} that provides various proxies and features.
 */
@Deprecated(since="6.0.7")
public class AxualKafkaProducerFactory<K, V> extends DefaultKafkaProducerFactory<K, V> {

    public AxualKafkaProducerFactory(Map<String, Object> configs) {
        super(configs);
    }

    @Override
    protected Producer<K, V> createKafkaProducer() {
        return new AxualProducer<>(getConfigurationProperties());
    }
}
