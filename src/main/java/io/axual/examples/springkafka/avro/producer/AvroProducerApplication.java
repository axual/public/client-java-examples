package io.axual.examples.springkafka.avro.producer;

/*-
 * #%L
 * axual-spring-kafka-avro-producer
 * %%
 * Copyright (C) 2022 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.ProducerListener;

import java.util.Collections;

import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.axual.client.example.schema.ApplicationLogLevel;

/**
 * <p>This spring boot application is a demonstration of a very simplistic AVRO Producer powered by
 * Spring Kafka that produces 1 message to the axual cluster. It can be cloned and ran directly.</p>
 *
 * <p>This example can be extended to suit your use case as needed.</p>
 */
@SpringBootApplication
@Profile("avro-producer")
@Deprecated(since="6.0.7")
public class AvroProducerApplication implements CommandLineRunner {
    private static final Logger LOG = LoggerFactory.getLogger(AvroProducerApplication.class);

    @Value("${axual.stream}")
    private String stream;

    private KafkaTemplate<Application, ApplicationLogEvent> kafkaTemplate;

    private static final Application SOURCE = Application.newBuilder()
            .setName("Axual ❤️ Spring")
            .setVersion("1.0.0")
            .setOwner("none")
            .build();

    @Autowired
    public AvroProducerApplication(KafkaTemplate<Application, ApplicationLogEvent> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public static void main(String[] args) {
        SpringApplication.run(AvroProducerApplication.class, args);
    }

    @Override
    public void run(String... args) {
        // every message in kafka consist of key and value, let's build it.
        Application key = Application.newBuilder()
                .setName("axual speaks spring")
                .setVersion("1.0.0")
                .setOwner("none")
                .build();

        ApplicationLogEvent value = ApplicationLogEvent.newBuilder()
                .setTimestamp(System.currentTimeMillis())
                .setSource(SOURCE)
                .setLevel(ApplicationLogLevel.INFO)
                .setMessage("Sending a message!")
                .setContext(Collections.singletonMap("Some key", "Some Value"))
                .build();

        kafkaTemplate.setProducerListener(new ProducerListener<Application, ApplicationLogEvent>() {
            @Override
            public void onSuccess(ProducerRecord<Application, ApplicationLogEvent> producerRecord,
                                  RecordMetadata recordMetadata) {
                // Do NOT perform time-consuming network or DB operations here. This method is
                // called within the producer sender thread so this method should complete super fast.
                // If you need to perform expensive network or DB operations store the metadata
                // in a hash map and perform the operation in a separate thread.
                long now = System.currentTimeMillis();
                LOG.info("Successfully produced message {} on partition {}, offset {} in {} milliseconds",
                        producerRecord, recordMetadata.partition(),
                        recordMetadata.offset(), now - recordMetadata.timestamp());
            }

            @Override
            public void onError(ProducerRecord<Application, ApplicationLogEvent> producerRecord,
                                RecordMetadata recordMetadata,
                                Exception exception) {
                // When a failure occurs, information is available in ExecutionException.
                LOG.error("Error occurred while producing message {}", producerRecord, exception);
            }
        });

        kafkaTemplate.send(stream, key, value);
    }
}
