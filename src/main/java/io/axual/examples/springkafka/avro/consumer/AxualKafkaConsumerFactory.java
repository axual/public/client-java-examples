package io.axual.examples.springkafka.avro.consumer;

/*-
 * #%L
 * axual-spring-kafka-avro-consumer
 * %%
 * Copyright (C) 2022 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.kafka.clients.consumer.Consumer;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

import java.util.Map;
import java.util.Properties;

import io.axual.client.proxy.axual.consumer.AxualConsumer;

/**
 * This factory extends the default factory provided by Spring Kafka and uses the {@link
 * AxualConsumer} that provides various proxies and features.
 */
@Deprecated(since="6.0.7")
public class AxualKafkaConsumerFactory<K, V> extends DefaultKafkaConsumerFactory<K, V> {

    public AxualKafkaConsumerFactory(Map<String, Object> configs) {
        super(configs);
    }

    @Override
    public Consumer<K, V> createConsumer(String groupId,
                                         String clientIdPrefix,
                                         String clientIdSuffixArg,
                                         Properties properties) {
        return new AxualConsumer<>(getConfigurationProperties());
    }

}
