package io.axual.examples.springkafka.avro.consumer;

/*-
 * #%L
 * axual-spring-kafka-avro-consumer
 * %%
 * Copyright (C) 2022 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;

import java.util.HashMap;
import java.util.Map;

import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.axual.client.proxy.axual.consumer.AxualConsumerConfig;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.client.proxy.generic.registry.ProxyTypeRegistry;
import io.axual.common.config.CommonConfig;
import io.axual.serde.avro.SpecificAvroDeserializer;

@EnableKafka
@Configuration
@Deprecated(since="6.0.7")
public class SpringAvroConsumer {

    @Value("${axual.endpoint}")
    private String endpoint;

    @Value("${axual.tenant}")
    private String tenant;

    @Value("${axual.environment}")
    private String environment;

    private final KafkaProperties kafkaProperties;

    @Autowired
    public SpringAvroConsumer(KafkaProperties kafkaProperties) {
        this.kafkaProperties = kafkaProperties;
    }

    @Bean
    public Map<String, Object> consumerConfigs() {
        Map<String, Object> props = new HashMap<>(kafkaProperties.buildConsumerProperties());

        /*
         * The Proxy chain config determines what added features you want when connecting to Axual.
         *
         * SWITCHING PROXY: Enables automatic failover to a different Kafka cluster. Team Speed can
         * trigger a switch when performing Kafka cluster maintenance. By passing the CommonConfig.ENDPOINT
         * config, the consumer discovers the Kafka broker coordinates automatically.
         *
         * RESOLVING PROXY: Enables resolution of topic and group id names to actual names in Kafka
         * cluster. This allows multi-tenancy and multi-environment support on a single Kafka cluster.
         *
         * LINEAGE PROXY: Adds useful metadata in message headers to allow tracing of messages through Axual.
         *
         * LOGGING PROXY: Can be activated anywhere in the chain to log the state.
         */
        props.put(AxualConsumerConfig.CHAIN_CONFIG, ProxyChain.newBuilder()
                .append(ProxyTypeRegistry.SWITCHING_PROXY_ID)
                .append(ProxyTypeRegistry.RESOLVING_PROXY_ID)
                .append(ProxyTypeRegistry.HEADER_PROXY_ID)
                .append(ProxyTypeRegistry.LINEAGE_PROXY_ID)
                .build());

        props.put(CommonConfig.APPLICATION_ID, kafkaProperties.getConsumer().getClientId());
        props.put(CommonConfig.APPLICATION_VERSION, "1.0.0");
        props.put(CommonConfig.TENANT, tenant);
        props.put(CommonConfig.ENVIRONMENT, environment);
        props.put(CommonConfig.ENDPOINT, endpoint);

        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, SpecificAvroDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, SpecificAvroDeserializer.class.getName());

        return props;
    }

    @Bean
    public ConsumerFactory<Application, ApplicationLogEvent> consumerFactory() {
        return new AxualKafkaConsumerFactory<>(consumerConfigs());
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<Application, ApplicationLogEvent> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<Application, ApplicationLogEvent> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        // Since Spring Kafka 2.3, container fails to start if topic is missing. This check is done by
        // initializing an Admin Client. We disable this to skip checking topic existence.
        factory.setMissingTopicsFatal(false);
        return factory;
    }


}
