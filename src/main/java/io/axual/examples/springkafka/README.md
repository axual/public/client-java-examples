# Client Spring Examples

This example repo shows typical uses of Axual Platform combined with [Spring Boot](https://spring.io/projects/spring-boot).

# Setting up the maven project
To enable Axual Client functionality within a Spring Boot application we need to include the following dependencies:

```xml
<!-- Axual Client. This is all we need to include from the Axual-Client. 
     The high level client is not necessary -->
<dependency>
   <groupId>io.axual.client</groupId>
   <artifactId>axual-client-proxy</artifactId>
</dependency>
<!-- Enables SpringBoot Kafka functionality -->
<dependency>
   <groupId>org.springframework.kafka</groupId>
   <artifactId>spring-kafka</artifactId>
</dependency>
<!-- Optionally when using Avro -->
<dependency>
   <groupId>org.apache.avro</groupId>
   <artifactId>avro</artifactId>
   <scope>compile</scope>
</dependency>
```

When using spring-kafka consumers, the `@EnableKafka` annotation is required in order to enable kafka-event listeners.

We define a `AxualKafkaProducer/ConsumerFactory<K, V>` bean which extendns `DefaultKafkaProducer/ConsumerFactory<K, V>`.
We do this in order to replace the implementation of the kafka producer/consumer with it's Axual counterpart.

## Configuration
Each application included comes with its own configuration which is used for the appropriate bean configuration injection.
You can configure those as you would any other profile, through your favorite IDE or using the command line argument, e.g. `-Dspring.profiles.active=avro-consumer`.
If the correct profile is not configured the application will not start since the classes annotated with `@SpringBootApplication` are conditionals on the appropriate profile being set.

<sup>*</sup> **Note**: If you want to run the examples against the SaaS trial environment, you will find information in the README.txt of care package, which has been sent to you when requesting your trial.

```yaml
spring:
  kafka:
    producer:
      client-id: io.axual.example.client.avro.producer
      acks: -1
      batch-size: 16834
      buffer-memory: 33554432
      properties:
        linger.ms: 100
      ssl:
        # Using JKS
        key-store-location: client-certs/axual.client.keystore.jks
        key-store-password: password
        key-password: password
        trust-store-location: client-certs/axual.client.truststore.jks
        trust-store-password: password
```

For additional information on the Axual Client Proxy consult the [documentation](https://docs.axual.io/client/5.7.2/how-to/kafkaproxy).

## Kafka Client Compatibility
In the following compatibility matrix we try to summarize testing that we have done and confirmed working

This table will be updated moving forward.

| Axual Client | kafka-clients | Spring Boot | spring-kafka |
|--------------|---------------|-------------|--------------|
| 5.7.x        | 2.7.x         | 2.5.x       | 2.7.x        |
| 6.0.x        | 3.2.x         | 2.7.x       | 2.8.x        |

## License
This project is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).
