package io.axual.examples.springkafka.string.consumer;

/*-
 * #%L
 * axual-spring-kafka-string-consumer
 * %%
 * Copyright (C) 2022 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;

/**
 * <p>This spring boot application is a demonstration of a very simplistic string Consumer powered by
 * Spring Kafka that produces 1 message to the axual cluster. It can be cloned and ran directly.</p>
 *
 * <p>This example can be extended to suit your use case as needed.</p>
 */
@SpringBootApplication
@Profile("string-consumer")
@Deprecated(since="6.0.7")
public class StringConsumerApplication {
    private static final Logger LOG = LoggerFactory.getLogger(StringConsumerApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(StringConsumerApplication.class, args);
    }

    @KafkaListener(topics = "#{'${axual.stream}'}")
    public void listen(ConsumerRecord<String, String> event) {
        long now = System.currentTimeMillis();
        LOG.info("Received message on topic {} partition {} offset {} key '{}' value '{}'. Time to consume {} ms",
                event.topic(), event.partition(), event.offset(), event.key(), event.value(), now - event.timestamp());
    }
}

