//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.examples.util;

import io.axual.common.config.ClientConfig;
import io.axual.common.config.CommonConfig;
import org.apache.kafka.clients.CommonClientConfigs;

import java.util.HashMap;
import java.util.Map;

import static io.axual.examples.util.config.Configuration.*;
import static io.axual.examples.util.KafkaSecurityUtil.addSASLConfig;
import static io.axual.examples.util.KafkaSecurityUtil.addSSLConfigUsingJKSStores;

public class Util {

    /** Utility method used to fetch keystores from the disk */
    public static String getResourceFilePath(String resource) {
        return ClassLoader.getSystemClassLoader().getResource(resource).getFile();
    }

    public static ClientConfig getClientConfig() {
        return ClientConfig.newBuilder()
                .setApplicationId(APPLICATION_ID)
                .setApplicationVersion(APPLICATION_VERSION)
                .setEndpoint(DISCOVERY_ENDPOINT)
                .setTenant(TENANT)
                .setEnvironment(ENVIRONMENT)
                .setSslConfig(AxualSSLUtil.getSSLConfigUsingJKSStores()) // See multiple ways to load SSL config in the `Axual SSL Util` class
                .setDisableTemporarySecurityFile(true)
                .build();
    }

    public static Map<String, Object> getAxualClientProxyConfig() {
        Map<String, Object> config = new HashMap<>();
        config.put(CommonConfig.APPLICATION_ID, APPLICATION_ID);
        config.put(CommonConfig.APPLICATION_VERSION, APPLICATION_VERSION);
        config.put(CommonConfig.TENANT, TENANT);
        config.put(CommonConfig.ENVIRONMENT, ENVIRONMENT);

        // Custom Axual property for Switching Proxy: This is the discovery API URL.
        // In its absence, BOOTSTRAP_SERVERS are considered to be the Discovery API URL. Best not to mix them up.
        config.put(CommonConfig.ENDPOINT, DISCOVERY_ENDPOINT);

        // Check the KafkaSecurityUtil class for multiple examples of configuring SSL
        addSSLConfigUsingJKSStores(config);

        return config;
    }

    public static Map<String, Object> getKafkaClientWithSSLConfig() {
        Map<String, Object> config = new HashMap<>();
        config.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS_SSL);

        // Check the KafkaSecurityUtil class for multiple examples of configuring SSL
        addSSLConfigUsingJKSStores(config);

        return config;
    }

    public static Map<String, Object> getKafkaClientWithSASLConfig() {
        Map<String, Object> config = new HashMap<>();
        config.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS_SASL);
        addSASLConfig(config);
        return config;
    }
}
