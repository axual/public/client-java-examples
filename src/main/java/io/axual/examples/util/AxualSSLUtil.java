//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.examples.util;

import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;

import static io.axual.examples.util.config.Configuration.CLIENT_KEYSTORE_PATH;
import static io.axual.examples.util.config.Configuration.KEY_MATERIAL_PASSWORD;
import static io.axual.examples.util.config.Configuration.CLIENT_TRUSTSTORE_PATH;
import static io.axual.examples.util.Util.getResourceFilePath;

public class AxualSSLUtil {

    // Option 1: using JKS type keystore & truststore
    public static SslConfig getSSLConfigUsingJKSStores() {
        return SslConfig.newBuilder()
                .setEnableHostnameVerification(false)
                .setKeystoreLocation(getResourceFilePath(CLIENT_KEYSTORE_PATH))
                .setKeystorePassword(KEY_MATERIAL_PASSWORD)
                .setKeyPassword(KEY_MATERIAL_PASSWORD)
                .setTruststoreLocation(getResourceFilePath(CLIENT_TRUSTSTORE_PATH))
                .setTruststorePassword(KEY_MATERIAL_PASSWORD)
                .build();
    }

    // Option 2: using p12 type keystore & truststore
    public static SslConfig getSSLConfigUsingP12Stores() {
        return SslConfig.newBuilder()
                .setEnableHostnameVerification(false)
                .setKeystoreType(SslConfig.KeystoreType.PKCS12)
                .setKeystoreLocation(getResourceFilePath("client-certs/axual.client.keystore.p12"))
                .setKeystorePassword(KEY_MATERIAL_PASSWORD)
                .setKeyPassword(KEY_MATERIAL_PASSWORD)
                .setTruststoreType(SslConfig.TruststoreType.PKCS12)
                .setTruststoreLocation(getResourceFilePath("client-certs/axual.client.truststore.p12"))
                .setTruststorePassword(KEY_MATERIAL_PASSWORD)
                .build();
    }

    // Option 3: using PEM formatted files for base64 encoded unencrypted keys and certificates
    public static SslConfig getSSLConfigUsingPemFiles() {
        return SslConfig.newBuilder()
                .setEnableHostnameVerification(false)
                .setKeystoreType(SslConfig.KeystoreType.PEM)
                .setKeystoreLocation(getResourceFilePath("client-certs/axual-client-keystore-keypair.pem"))
                .setKeyPassword(KEY_MATERIAL_PASSWORD)
                .setTruststoreType(SslConfig.TruststoreType.PEM)
                .setTruststoreLocation(getResourceFilePath("client-certs/axual-client-truststore-certificates.cer"))
                .build();
    }

    // Option 4: using PEM formatted strings for base64 encoded unencrypted keys and certificates
    public static SslConfig getSSLConfigUsingPEMStrings() {
        // These strings can come from anywhere stored securely. It's not advised to have hardcoded strings like these.
        String keystoreCertificateChain = "-----BEGIN CERTIFICATE-----\nMIIDkjCCAnoCCQD8145Kq9TyjDANBgkqhkiG9w0BAQUFADCBhTELMAkGA1UEBhMC\nTkwxFjAUBgNVBAgMDU5vcnRoIEJyYWJhbnQxFjAUBgNVBAcMDVJhYW1zZG9ua3Zl\nZXIxDjAMBgNVBAoMBUF4dWFsMR0wGwYDVQQLDBRQbGF0Zm9ybSBEZXZlbG9wbWVu\ndDEXMBUGA1UEAwwObG9jYWwuYXh1YWwuaW8wHhcNMTgwNjI2MTcyMTA3WhcNMjgw\nNjIzMTcyMTA3WjCBjzELMAkGA1UEBhMCTkwxFjAUBgNVBAgTDU5vcnRoIEJyYWJh\nbnQxFjAUBgNVBAcTDVJhYW1zZG9ua3ZlZXIxDjAMBgNVBAoTBUF4dWFsMR0wGwYD\nVQQLExRQbGF0Zm9ybSBEZXZlbG9wbWVudDEhMB8GA1UEAxMYQXh1YWwgUGxhdGZv\ncm0gRGV2ZWxvcGVyMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsSoK\ns7ADOa8FX4qU4V3SdwoSvxoTa7u9CEcyNzx5TZ2iUrAh5r+Gb/RgI7exVrBXNbSq\nRCdNtJLnpJ+43buuJIJf/enAuPMnMrfS3lba1O31pfPM9+5imK83I5nFwsAgJXTi\n0WmCTkjLRE1QF0PflHBRfseN+Br9K44xqJvzPQEj7a3ZVv4W1XhKSYf5P6R5/s9+\n+KrI+/a4pvJB4VkyPEO9fXf1eK6e4BCSfGC5TwrRhDTQPrrlFVRf7PT/DoDeZrVu\nk9L09qOJyfrOzIsmVH8IE1h3ilrJDiMOd+sdc85e9KfuVbPIM4tepjzzHOctpUAn\n93Wl0Tk7Qn5NPdusKwIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQAQXfxTJkm2pVH1\naxuebosQbUw5KYYVWcMX/3MPywbqnEcn0bRvr/p0u3652Ajv5nLKk18zG/3fOhaa\nHuBTP1TILExVHxZwG5dte/DWX2HaVqx6z1y8Y9Y89B0LazFmrD0ToiI6EusmHzKY\nbLXg2oouNprhh7q980ksSuy5fazBll0RlOsvyz7rDBwuOqEATjB6TCkxaRd4pgZI\nDknnrE6ySsvBggreAtaLa1AC+tYH1Lzw05Rwkqh5hFKygi6Ay5/5eFAA2cLVxZkc\nYpzPkS5S0VtraOj7M42cvnv8WCnpwu1OIYyH1uMculTRZ3FMztob5hXsuR5afV19\n/wpgAW75\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\nMIIDiDCCAnACCQDF8fbaJl/f3DANBgkqhkiG9w0BAQsFADCBhTELMAkGA1UEBhMC\nTkwxFjAUBgNVBAgMDU5vcnRoIEJyYWJhbnQxFjAUBgNVBAcMDVJhYW1zZG9ua3Zl\nZXIxDjAMBgNVBAoMBUF4dWFsMR0wGwYDVQQLDBRQbGF0Zm9ybSBEZXZlbG9wbWVu\ndDEXMBUGA1UEAwwObG9jYWwuYXh1YWwuaW8wHhcNMTgwNjI2MTcxOTIwWhcNMjgw\nNjIzMTcxOTIwWjCBhTELMAkGA1UEBhMCTkwxFjAUBgNVBAgMDU5vcnRoIEJyYWJh\nbnQxFjAUBgNVBAcMDVJhYW1zZG9ua3ZlZXIxDjAMBgNVBAoMBUF4dWFsMR0wGwYD\nVQQLDBRQbGF0Zm9ybSBEZXZlbG9wbWVudDEXMBUGA1UEAwwObG9jYWwuYXh1YWwu\naW8wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCdmUKbxLc59vNCKKOa\nieFo26uGRD7Q1AKmFyb6ZbnhkxKNqMs+DWCRwSS5b/Tw9yQ8jc3iuXor3vyHK3pu\n5LvDINBbwXOEHbEnZrqxQXK5fLy3Jm+XhO5IL1V2GGAUYvrousDzlhmrwnc1i0Bt\nX2jGkDgP3if1G+fdmobrDgL7bDJ59l33F7yrdgkaiNwPDuApAbWt+ZyeQDZ3oNLG\nEnMOQTv4iO2VHVziB/J0tj1Bixt+jVgriFpgptYQmcjRLPo1LZln3fdp3NtoaMe6\nYUYHHUV5TAfU23EQ24VJvgOUs+A/iplXgsWdOfPnmr9EXj+QZFPlZJ2wUz/1/z+G\nSh9BAgMBAAEwDQYJKoZIhvcNAQELBQADggEBABASw+75Bo+zPeTU7KLjws+Tzpvz\nYHzBbTOjJFz+IXfOKatbMh3pjepLmmABNdlQ2L6aqO/0qFfGv2abWvF/+ok5zWNn\nN/peZxcxe3u7OUrkCJuUEHYX0TgltR9bRbnbnqjLUq4vKzQ06KKFbW0HTbtC2vk7\nJPUDviqinjEjI2CsafiZAf0grXEzmD8nj+SNafrWf8F3pyivn6FuQ0XbBVptMZrS\numc8YqsHJXhdA6MYQGapMnv9GYG+Ka0SbuwmkFy0kRpDOU8MQy0tYK3WszoSRT+O\nDb4XaRAufP985vahvC6aSR7LnvRezNgADfmKbZl5jVFXG/MV/JyS6mtm8GA=\n-----END CERTIFICATE-----\n";
        String keystoreKey = "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCxKgqzsAM5rwVf\nipThXdJ3ChK/GhNru70IRzI3PHlNnaJSsCHmv4Zv9GAjt7FWsFc1tKpEJ020kuek\nn7jdu64kgl/96cC48ycyt9LeVtrU7fWl88z37mKYrzcjmcXCwCAldOLRaYJOSMtE\nTVAXQ9+UcFF+x434Gv0rjjGom/M9ASPtrdlW/hbVeEpJh/k/pHn+z374qsj79rim\n8kHhWTI8Q719d/V4rp7gEJJ8YLlPCtGENNA+uuUVVF/s9P8OgN5mtW6T0vT2o4nJ\n+s7MiyZUfwgTWHeKWskOIw536x1zzl70p+5Vs8gzi16mPPMc5y2lQCf3daXROTtC\nfk0926wrAgMBAAECggEAaAC/QZcdfZqCdAD9v4N9j0ZJlQgwyHjw0tBA6W5F48ub\nRCGD9VsQB98VJUKsB7EDsVJ69gGAu3XWKK1fMEQCSgqDYaL88VZE96A0WTPxyThc\nkeyash2uoeWSYALgtqBk/rgsgzUGOwC+2zzrvIyqzxBUtzFc5X6qiwwxmMLcOz3Y\nv+bP0KcsCl/uHHpjgPYqQhpCl1G8ErbMA8sgyzb+X6RhxHrz22+1jkxgL/cGUubb\n+g1HyxdUAKKK8R1MTgsYVW6XOOFWPCn7hDmFzZtckxTdrA+66rM/OmZLRoww9vQZ\nerGiH1Uv7GEdBYhYqseEJZ1PxvK3O6YC78jcffq9uQKBgQD0Lw9vZo1hDHG7Lmt0\nXqAMtOwY17CQ4tQRrKkFPboqSqKThdXQ9P+sjo1YU6wGNTW6NiI+5zcEzC6sWatc\nRrZKfBra0E1+n5vCT/yYepfF+3xC4wfFhEuZPFGQIdh0XGmhkT0DukfqLWH2XiG+\nOHvUxq+EN/ITCdN50zZidXBjJQKBgQC5vL7LTep+dBZYg31ZMx3j70OGYIH/lttz\nDW3BwdNBJp+vG8Q8TnsSZ25v0kaB1cYdGDTge3WW3ZTaXSfWWnutuU4BTI6eD6UH\ncLJ0vZd04Qk00ttbzgE6JiQm4HrhBHZgcJQbJEkogV+6Y0/mAFu7DG2XH6r6LFnB\nKBWT4DZZDwKBgCgpUVWWPWyX2mDZ+qxyH8rXOvm/B/hchlq91jLZezQXgHPZEFjE\n4wRjkdXUNTf0KnkNDEbiSodMeeS4/tk3fCX2EYipuAU6hSjJdRczGqFigoaRxqZy\n4ug6JoQZPPuuc2UyeSGS0t8uRa16v/wEWEGfyCBr/zGobRLdbVV2UVzNAoGAOSV9\nfofmkimdhnZOZtd3Zt4C5KFk3gLIWknTbz33haAgmXvtkLCE5VC1heooj2H6ppEA\nE+FoeJaMafMngqgsTXMqMPQhHTirCfL+tTRwGSHz9zC5FTH45q89iEihBgKdeWap\n6v/rEm9byLktqBKMJqzYOxsfPAHRS8DNgsYFcrkCgYA/JmRzs6jhdpn9Q6aKGejz\naLhdvmSio1xL523FGARIqYJHFjBse9lcg/Khd1pUJpCFWkDdMLzPzw1Hi2wJEWZo\nxqHQBqF0YW+ylzIMdTh4mdnOwRnDRBh2rmpVwJCX9+5cwzjAiEW+xitz9hVfPAQx\nvDUOeh3W/XCcwh/Y2R9Klw==\n-----END PRIVATE KEY-----\n";
        String trustedCertificates = "-----BEGIN CERTIFICATE-----\nMIIDiDCCAnACCQDF8fbaJl/f3DANBgkqhkiG9w0BAQsFADCBhTELMAkGA1UEBhMC\nTkwxFjAUBgNVBAgMDU5vcnRoIEJyYWJhbnQxFjAUBgNVBAcMDVJhYW1zZG9ua3Zl\nZXIxDjAMBgNVBAoMBUF4dWFsMR0wGwYDVQQLDBRQbGF0Zm9ybSBEZXZlbG9wbWVu\ndDEXMBUGA1UEAwwObG9jYWwuYXh1YWwuaW8wHhcNMTgwNjI2MTcxOTIwWhcNMjgw\nNjIzMTcxOTIwWjCBhTELMAkGA1UEBhMCTkwxFjAUBgNVBAgMDU5vcnRoIEJyYWJh\nbnQxFjAUBgNVBAcMDVJhYW1zZG9ua3ZlZXIxDjAMBgNVBAoMBUF4dWFsMR0wGwYD\nVQQLDBRQbGF0Zm9ybSBEZXZlbG9wbWVudDEXMBUGA1UEAwwObG9jYWwuYXh1YWwu\naW8wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCdmUKbxLc59vNCKKOa\nieFo26uGRD7Q1AKmFyb6ZbnhkxKNqMs+DWCRwSS5b/Tw9yQ8jc3iuXor3vyHK3pu\n5LvDINBbwXOEHbEnZrqxQXK5fLy3Jm+XhO5IL1V2GGAUYvrousDzlhmrwnc1i0Bt\nX2jGkDgP3if1G+fdmobrDgL7bDJ59l33F7yrdgkaiNwPDuApAbWt+ZyeQDZ3oNLG\nEnMOQTv4iO2VHVziB/J0tj1Bixt+jVgriFpgptYQmcjRLPo1LZln3fdp3NtoaMe6\nYUYHHUV5TAfU23EQ24VJvgOUs+A/iplXgsWdOfPnmr9EXj+QZFPlZJ2wUz/1/z+G\nSh9BAgMBAAEwDQYJKoZIhvcNAQELBQADggEBABASw+75Bo+zPeTU7KLjws+Tzpvz\nYHzBbTOjJFz+IXfOKatbMh3pjepLmmABNdlQ2L6aqO/0qFfGv2abWvF/+ok5zWNn\nN/peZxcxe3u7OUrkCJuUEHYX0TgltR9bRbnbnqjLUq4vKzQ06KKFbW0HTbtC2vk7\nJPUDviqinjEjI2CsafiZAf0grXEzmD8nj+SNafrWf8F3pyivn6FuQ0XbBVptMZrS\numc8YqsHJXhdA6MYQGapMnv9GYG+Ka0SbuwmkFy0kRpDOU8MQy0tYK3WszoSRT+O\nDb4XaRAufP985vahvC6aSR7LnvRezNgADfmKbZl5jVFXG/MV/JyS6mtm8GA=\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\nMIIFJjCCAw6gAwIBAgIJAINuAirfnRU6MA0GCSqGSIb3DQEBCwUAMCAxHjAcBgNV\nBAMMFUF4dWFsIER1bW15IFJvb3QgMjAxODAeFw0xODA1MjkxMDM0MTRaFw0zODA1\nMjQxMDM0MTRaMCAxHjAcBgNVBAMMFUF4dWFsIER1bW15IFJvb3QgMjAxODCCAiIw\nDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAMVDjbhq3TGuQ6INTZ+dhSIgsdbq\nw2nxF3myrS7v89bcNxMyLypWYTmR4OAYRXRBnW4KX6sTubPyL3ogPz6hXmfmPfAz\n+X//HTIiybL3e3qwxqWphp09+JT6veEp/e/wEEjSMj5nsxkDEjj9JEQWu/1B+N+V\nXOJkTYFy05ZgeWplkyLwT71myF047aISK27a+VebBMaPpvvetScbMSwxAbk51cGV\nUC4gpwvnvsbp/CRuMV0dYzkeTmxgn860l3s8+7qUJoOrtiO0cDpv97SK9Ck9ef1k\nR6KFttzxb/u+eMFi3RUErEGwE8P3thTseXRkp5hMwcyaSQv0wfLawlwcNFGOzsBx\nfJS7QUIUpEyzRqj5Ppgaj530APxbgitLOfVLZ2DvcBcmnQns6OE+uwymuvAj8Ftj\n6AFJXH2lmswHLl5uD9kIOwmpZg4NZLP2Qv+WOT6HLgI7Kv1z0OV2H7UlWA7hwQXl\noQ6fJ2YLEhT+GM9xHKJ+DQCxvjWvtGUSb/Dk0j/R9mpSFfHvVJgE/xV+7F7Vlyw5\n/cDpF3GZOTGQ/MFy4RqRrTtjnZw2/bZZyJ+Xb743OeQhABFUdadh8cmyehDregtr\nalHxtjKxCxrT55OHCYhbCoz6nEnQURD7EPQhU5puUKalRq2ApDkveIk8uj0HQmQm\nKyRuNX7M6vCoWnpxAgMBAAGjYzBhMB0GA1UdDgQWBBR0o48OpIVDoF+TQj9qwGQH\nK3mCxTAfBgNVHSMEGDAWgBR0o48OpIVDoF+TQj9qwGQHK3mCxTAPBgNVHRMBAf8E\nBTADAQH/MA4GA1UdDwEB/wQEAwIBhjANBgkqhkiG9w0BAQsFAAOCAgEAbJanqR4P\nmr05AyAu8vlrLsleXA8VAPDiaaYStYH5cIdBBWkaIxanLFDmbyQwKkKdkHQWV9X8\n1P52q49T9RsoBsEOmwdiaCY2PEUz7Y3bFW0UeM+k65VlHlXWywRM6+O02t4TrJXH\nF6h7vPon01OwhgW9Yil/Kr+yyZK50Ic+pm4UhHmtxY932cNaRCdae5tKsjabsP7Z\nrdAksLia8mTp+HADkZJ1uODxyDh0S1WMKB5JoHYBrmtUr1NYLgRC6SinhK4r7rbi\nEWuurE605Nm//jv3Czdy8gEsMDtXLZYY0iqGnD11MAJFXyQ6PG2eq1cXcsJNRojm\n8D4ipfQ+z4bp9dDVR2DzVyTYe4yuhZuIe2phOhPc8KkBaXQRMHfVKyeEmzqEFLaM\nkfaDZkRsrMZSqh+KJoxDG3h8UqssChX+cuZdsjRhNWRqfbB20I9Upwa+XooyCU4E\nEkYyFTMchtvbYZEN/XvlPfhK5JB9eJ5rrcE8hKsP3gftchWWqCDedKugvZW/t5Vk\nlc+z4IjiJFnRDfcr4Z5V2Hpseyno3AEK7aUdJlmuPnxoImFXfQ4jUguM/wznJHl7\nXv9T0oaBVHM7Bd6PlES04Oho0KZXS6NryTsZn9GFV4qGZj5lEeOVl15AOfeIjP/I\nokA2uUH/ZuJlR/BEmqbLt5HWPRNT/GgLfPY=\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\nMIIFLTCCAxWgAwIBAgICEAAwDQYJKoZIhvcNAQELBQAwIDEeMBwGA1UEAwwVQXh1\nYWwgRHVtbXkgUm9vdCAyMDE4MB4XDTE4MDUyOTExMDEzNFoXDTI4MDUyNjExMDEz\nNFowKzEpMCcGA1UEAwwgQXh1YWwgRHVtbXkgSW50ZXJtZWRpYXRlIDIwMTggMDEw\nggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQC9uOuzJekmeo3hl8fjQlKS\nHApS3llcliq1YrXpkMbHAA9StHaMHPW+Dzr2/+cdfBAmN3sujCY8Paq15QI+TDOq\nKA5SByCBQKXx2qulBPcZs3mDMt+KxAaeWfwR4Nj0NNKbmw2HjDddo77joeVOuOX2\n4o1wXzmAAolVMIcRYA11EMWNUtYrHCzBa7RfYht2G5dE69ckrgfw1Nxs01Sbg+xP\nsK9aK/LHPUalYZNY+76x7vabEpzaPfpyKzDTWA20SPk0WfTf9/+K3o+urzDG8O/q\nw9xbBOzWohGmRyA/z841p1SD7inpZcyO/KeW1yTP2WyFxADwUrv2mEYXnma/Gdna\nG62IQYk/UMex9W8pT6tfwrg/36sSwr88yPR5dJxzjHUE+w/rYG3k+K+EqvZ5qOC5\n32AJ9BS2nbNuGpmRU1qoMCwpL7B2E/CKJLIdFcf/qmcnWJEXo+u34+fQZg8XaDCI\nXhUqAHz6YkjCiFGd/JwL1IqsfxFsV9wHTUbW2AumglU65ZrjhXrrzE7Hk9ng1spJ\ndOwfBihBNjnr0mKHY9leJ3chJ9HQ55/fEgcRNrj8EC69QCeAtpY5yOAjKpA03UvF\ngrDt8CIyIehNUwTXIhQSHZU4eZ0rzWf0vvMbhL2FvKtphbpnNKoXeNLv2IMZpT4B\nVwsqLqaIkl/I4FPpYBoSYwIDAQABo2YwZDAdBgNVHQ4EFgQUa9IpV4tSNiwFCsZX\nuRp0eKwTH2YwHwYDVR0jBBgwFoAUdKOPDqSFQ6Bfk0I/asBkByt5gsUwEgYDVR0T\nAQH/BAgwBgEB/wIBADAOBgNVHQ8BAf8EBAMCAYYwDQYJKoZIhvcNAQELBQADggIB\nAKoNIqiOdjlUBNg7cvR9Su4KgGrsZf78pG1H2MlNxJjFM/80EiWEfze/EG2MLxFq\n8vToIHDjb0kVetYpdmfHNXTTlaaroBlXwyUYToPzQ985qr3LD8RhYZFAsiZCTtpJ\n4FT6sh/mccTyx8G8+ZS6mn/le2WPj/t6beNLgbdl5n8fghdQcmT/TqGXE50UftWt\nHSx3fsq2aKuNdVzhKzTin50IbiE9DV1dKo6B+ipOy/Dz5GMv3Z/3ntLTvxabCMOl\n7s7WsUE7VPABRSifUS80Z9Ai38faLSu+Ouzx40ceXwvlFQtJ2LYQ8Ru5Q63k2wB3\nEOE6cgAhiYExrz3fDDtUkui9vIfWfTPMnXR7xQ8YqK4Qqld2ESxvMQU2jzbZKSf+\n3sWnPvN4HTg0cfysmOdLGZwf3u8A9tMtxhUEtxUx7r76M4ekSKdNv1Nf5u5N/h7b\nAbEqSp1XADTxkE448i7hNJzn2Ce6JtFya231Ni0xyYKQIajP18jNypAw1eABYFkN\n53vQTUfqcbtcrCios1xRdDqfgkYaKZv7p63aoObFTf/mmG7sFjGAEPQscagOukwN\nwnkjCVifVbk5qJUaUWSLeYziI+HYkEA9P/h4o83nbf0YgBtOFoc0XWKmKagHifZN\nSEJ9kRCWzYaL2ChiL6jHGh26WT/hbNKeAlcxPnT4u/l1\n-----END CERTIFICATE-----\n";

        return SslConfig.newBuilder()
                .setEnableHostnameVerification(false)
                .setKeystoreType(SslConfig.KeystoreType.PEM)
                .setKeystoreCertificateChain(keystoreCertificateChain)
                .setKeystoreKey(new PasswordConfig(keystoreKey))
                .setTruststoreType(SslConfig.TruststoreType.PEM)
                .setTruststoreCertificates(trustedCertificates)
                .build();
    }
}
