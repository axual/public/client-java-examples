package io.axual.examples.util.config.impl;

import io.axual.examples.util.config.Configuration;

public class HelmConfiguration implements Configuration {

    static final String TENANT_HELM = "axual";
    static final String INSTANCE_HELM = "local";
    static final String ENVIRONMENT_HELM = "example";

    // SSL Certificates configuration
    static final String CLIENT_KEYSTORE_PATH_HELM = "client-certs/axual.client.keystore.helm.jks";
    static final String CLIENT_TRUSTSTORE_PATH_HELM = "client-certs/axual.client.truststore.helm.jks";
    static final String KEY_MATERIAL_PASSWORD_HELM = "notsecret";

    static final String GROUP_ID_RESOLVED_HELM = TENANT_HELM + '-' + INSTANCE_HELM + '-' + ENVIRONMENT_HELM + '-' + APPLICATION_ID;
    static final String TOPIC_RESOLVED_NAME_STRING_HELM = TENANT_HELM + '-' + INSTANCE_HELM + '-' + ENVIRONMENT_HELM + '-' + TOPIC_NAME_STRING;
    static final String TOPIC_RESOLVED_NAME_AVRO_HELM = TENANT_HELM + '-' + INSTANCE_HELM + '-' + ENVIRONMENT_HELM + '-' + TOPIC_NAME_AVRO;

    // Connectivity configuration - Local platform deployment
    public static final String DISCOVERY_HELM = "https://platform.local:29000";
    public static final String SCHEMA_REGISTRY_HELM = "https://platform.local:25000";
    public static final String BOOTSTRAP_SERVERS_SSL_HELM = "platform.local:31757";
    public static final String BOOTSTRAP_SERVERS_SASL_HELM = "platform.local:31758";

    @Override
    public String getTenant() { return TENANT_HELM; }

    @Override
    public String getInstance() { return INSTANCE_HELM; }

    @Override
    public String getEnvironment() { return ENVIRONMENT_HELM; }

    @Override
    public String getClientKeystorePath() { return CLIENT_KEYSTORE_PATH_HELM; }

    @Override
    public String getClientTruststorePath() { return CLIENT_TRUSTSTORE_PATH_HELM; }

    @Override
    public String getGroupIdResolved() { return GROUP_ID_RESOLVED_HELM; }

    @Override
    public String getTopicResolvedAvro() { return TOPIC_RESOLVED_NAME_AVRO_HELM; }

    @Override
    public String getTopicResolvedString() { return TOPIC_RESOLVED_NAME_STRING_HELM; }

    @Override
    public String getKeyMaterialPassword() { return KEY_MATERIAL_PASSWORD_HELM; }

    @Override
    public String getDiscoveryEndpoint() { return DISCOVERY_HELM; }

    @Override
    public String getSchemaRegistry() { return SCHEMA_REGISTRY_HELM; }

    @Override
    public String getBootstrapServersSSL() { return BOOTSTRAP_SERVERS_SSL_HELM; }

    @Override
    public String getBootstrapServersSASL() { return BOOTSTRAP_SERVERS_SASL_HELM; }

}
