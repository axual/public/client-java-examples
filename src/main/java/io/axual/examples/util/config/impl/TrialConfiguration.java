package io.axual.examples.util.config.impl;

import io.axual.examples.util.config.Configuration;

public class TrialConfiguration implements Configuration {

    static final String TENANT_TRIAL = "axual";
    static final String INSTANCE_TRIAL = "local";
    static final String ENVIRONMENT_TRIAL = "default";

    // SSL Certificates configuration
    static final String CLIENT_KEYSTORE_PATH_TRIAL = "client-certs/axual.client.keystore.jks";
    static final String CLIENT_TRUSTSTORE_PATH_TRIAL = "client-certs/axual.client.truststore.jks";
    static final String KEY_MATERIAL_PASSWORD_TRIAL = "password";

    static final String GROUP_ID_RESOLVED_TRIAL = TENANT_TRIAL + '-' + INSTANCE_TRIAL + '-' + ENVIRONMENT_TRIAL + '-' + APPLICATION_ID;
    static final String TOPIC_RESOLVED_NAME_STRING_TRIAL = TENANT_TRIAL + '-' + INSTANCE_TRIAL + '-' + ENVIRONMENT_TRIAL + '-' + TOPIC_NAME_STRING;
    static final String TOPIC_RESOLVED_NAME_AVRO_TRIAL = TENANT_TRIAL + '-' + INSTANCE_TRIAL + '-' + ENVIRONMENT_TRIAL + '-' + TOPIC_NAME_AVRO;

    // Connectivity configuration - Axual Trial
    static final String DISCOVERY_TRIAL = "https://" + TENANT_TRIAL + "-" + INSTANCE_TRIAL + "-discoveryapi.axual.cloud";
    static final String SCHEMA_REGISTRY_TRIAL = "https://" + TENANT_TRIAL + "-" + INSTANCE_TRIAL + "-schemas.axual.cloud";
    static final String BOOTSTRAP_SERVERS_SSL_TRIAL = "oberon-kafka-bootstrap.axual.cloud:9094";
    static final String BOOTSTRAP_SERVERS_SASL_TRIAL = "oberon-kafka-sasl-bootstrap.axual.cloud:9095";

    @Override
    public String getTenant() { return TENANT_TRIAL; }

    @Override
    public String getInstance() { return INSTANCE_TRIAL; }

    @Override
    public String getEnvironment() { return ENVIRONMENT_TRIAL; }

    @Override
    public String getClientKeystorePath() { return CLIENT_KEYSTORE_PATH_TRIAL; }

    @Override
    public String getClientTruststorePath() { return CLIENT_TRUSTSTORE_PATH_TRIAL; }

    @Override
    public String getGroupIdResolved() { return GROUP_ID_RESOLVED_TRIAL; }

    @Override
    public String getTopicResolvedAvro() { return TOPIC_RESOLVED_NAME_AVRO_TRIAL; }

    @Override
    public String getTopicResolvedString() { return TOPIC_RESOLVED_NAME_STRING_TRIAL; }

    @Override
    public String getKeyMaterialPassword() { return KEY_MATERIAL_PASSWORD_TRIAL; }

    @Override
    public String getDiscoveryEndpoint() { return DISCOVERY_TRIAL; }

    @Override
    public String getSchemaRegistry() { return SCHEMA_REGISTRY_TRIAL; }

    @Override
    public String getBootstrapServersSSL() { return BOOTSTRAP_SERVERS_SSL_TRIAL; }

    @Override
    public String getBootstrapServersSASL() { return BOOTSTRAP_SERVERS_SASL_TRIAL; }

}
