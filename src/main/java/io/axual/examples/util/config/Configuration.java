package io.axual.examples.util.config;

import io.axual.examples.util.config.impl.TrialConfiguration;

public interface Configuration {

    String getTenant();
    String getInstance();
    String getEnvironment();
    String getClientKeystorePath();
    String getClientTruststorePath();
    String getGroupIdResolved();
    String getTopicResolvedAvro();
    String getTopicResolvedString();
    String getKeyMaterialPassword();
    String getDiscoveryEndpoint();
    String getSchemaRegistry();
    String getBootstrapServersSSL();
    String getBootstrapServersSASL();

    // Here you can configure which environment you want to connect to
    //                                        (can be HelmConfiguration() or TrialConfiguration()):
    Configuration configuration = new TrialConfiguration();
//    Configuration configuration = new HelmConfiguration();

    // Globals
    // SASL credentials which have been generated in Self-Service
    String SASL_USERNAME = "username";
    String SASL_PASSWORD = "password";

    // Application configuration
    String APPLICATION_ID = "io.axual.examples";

    String APPLICATION_VERSION = "1.0.0";
    String APPLICATION_NAME = "Axual Client Examples";
    String APPLICATION_OWNER = "default";

    String TOPIC_NAME_STRING = "example-string-stream";
    String TOPIC_NAME_AVRO= "applicationlogevents";

    String DISCOVERY_ENDPOINT = configuration.getDiscoveryEndpoint();
    String SCHEMA_REGISTRY = configuration.getSchemaRegistry();
    String BOOTSTRAP_SERVERS_SSL = configuration.getBootstrapServersSSL();
    String BOOTSTRAP_SERVERS_SASL = configuration.getBootstrapServersSASL();

    String TENANT = configuration.getTenant();
    String INSTANCE = configuration.getInstance();
    String ENVIRONMENT = configuration.getEnvironment();

    String TOPIC_RESOLVED_NAME_AVRO = configuration.getTopicResolvedAvro();
    String TOPIC_RESOLVED_NAME_STRING = configuration.getTopicResolvedString();
    String GROUP_ID_RESOLVED = configuration.getGroupIdResolved();

    String CLIENT_KEYSTORE_PATH = configuration.getClientKeystorePath();
    String CLIENT_TRUSTSTORE_PATH = configuration.getClientTruststorePath();
    String KEY_MATERIAL_PASSWORD = configuration.getKeyMaterialPassword();

}
