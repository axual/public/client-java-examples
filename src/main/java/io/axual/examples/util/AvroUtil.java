//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.examples.util;

import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogAlertSetting;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.axual.client.example.schema.ApplicationLogLevel;

import java.util.Collections;
import java.util.List;

import static io.axual.examples.util.config.Configuration.*;

public class AvroUtil {

    /** Utility AVRO method which builds an example object. */
    public static Application buildApplication(String name) {
        return Application.newBuilder()
                .setName(APPLICATION_NAME + " - " + name)
                .setVersion(APPLICATION_VERSION)
                .setOwner(APPLICATION_OWNER)
                .build();
    }

    /** Utility AVRO method which builds an example object. */
    public static ApplicationLogEvent buildApplicationLogEvent(Application application, String message) {
        return ApplicationLogEvent.newBuilder()
                .setTimestamp(System.currentTimeMillis())
                .setSource(application)
                .setLevel(io.axual.client.example.schema.ApplicationLogLevel.INFO)
                .setMessage(message)
                .setContext(Collections.singletonMap("Some key", "Some Value"))
                .build();
    }

    public static ApplicationLogAlertSetting buildApplicationLogAlertSetting(String source) {
        return ApplicationLogAlertSetting.newBuilder()
                .setSource(source)
                .setAddresses(List.of("address"))
                .setLogLevels(List.of(ApplicationLogLevel.INFO))
                .build();
    }
}
