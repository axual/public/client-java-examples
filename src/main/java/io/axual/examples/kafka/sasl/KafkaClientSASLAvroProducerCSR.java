//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.examples.kafka.sasl;

import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.axual.common.tools.SleepUtil;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.security.auth.SecurityProtocol;
import org.apache.kafka.common.security.scram.internals.ScramMechanism;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static io.axual.examples.util.AvroUtil.buildApplication;
import static io.axual.examples.util.AvroUtil.buildApplicationLogEvent;
import static io.axual.examples.util.config.Configuration.APPLICATION_ID;
import static io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig.AUTO_REGISTER_SCHEMAS;
import static java.time.Instant.now;
import static org.apache.kafka.clients.CommonClientConfigs.SECURITY_PROTOCOL_CONFIG;
import static org.apache.kafka.common.config.SaslConfigs.SASL_JAAS_CONFIG;
import static org.apache.kafka.common.config.SaslConfigs.SASL_MECHANISM;

import static io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG;


/**
 * Kafka Client Example (Producer, Avro, Confluent Avro SerDes)
 */
public class KafkaClientSASLAvroProducerCSR {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaClientSASLAvroProducerCSR.class);
    private static final String JAAS_TEMPLATE = "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"%s\" password=\"%s\";";
    private static String appUsername;
    private static String appPassword;

    public static void main(String[] args) {
        Map<String, Object> config = new HashMap<>();
        // STEP 1: insert the name of the topic you created using Self-Service.
        // You can find the topic name by clicking the "i" icon on the topic detail page.
        String topicName = "insert-topic-name-here";

        // STEP 2: insert the credentials that you created using Self-service
        appUsername = "username"; // "Username" in the Self-Service user interface
        appPassword = "password"; // "Password" in the Self-Service user interface

        // Note: modify the BOOTSTRAP_SERVERS_CONFIG below if you are *not* connecting to the
        // shared Kafka environment provided by Axual
        // we are using Let's encrypt CA to sign the bootstrap server certificate
        config.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, "bootstrap-sharedkafka.byok.p.westeurope.azure.axual.cloud:9095");
        addSASLConfig(config);

        // Prepare producer-specific configuration
        // Client.ID config
        //
        // An id string to pass to the server when making requests.
        // The purpose of this is to be able to track the source of requests beyond
        // just ip/port by allowing a logical application name to be included in server-side request logging.
        config.put(ProducerConfig.CLIENT_ID_CONFIG, APPLICATION_ID);

        // Serialization config
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);

        // STEP 3: insert the "Confluent SR (Schema Registry) URL" here
        config.put(SCHEMA_REGISTRY_URL_CONFIG, "insert-schema-registry-url-here");

        config.put(AUTO_REGISTER_SCHEMAS, false);

        // "Delivery & Ordering strategy"
        config.put(ProducerConfig.ACKS_CONFIG, "-1");
        config.put(ProducerConfig.RETRIES_CONFIG, "2");

        // Batching config
        config.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "1");

        // Timeout values
        config.put(ProducerConfig.RETRY_BACKOFF_MS_CONFIG, "1000"); // Only relevant when RETRIES_CONFIG > 0
        config.put(ProducerConfig.RECONNECT_BACKOFF_MAX_MS_CONFIG, "1000");

        // Instantiate the Kafka producer, for which we prepared the configuration at the beginning.
        // Using try-with-resources since it implements the closeable interface.
        // In production, you wouldn't use try with resources:
        //     you would instead instantiate it once and REUSE it throughout the project's lifetime,
        //     finally closing it during application shutdown.
        LOG.info("Instantiating producer with properties: {}", config);
        try (Producer<Application, ApplicationLogEvent> producer = new KafkaProducer<>(config)) {
            LOG.info("Producer instantiated successfully.");

            // This string will be included in every message for aspect only
            Application key = buildApplication(KafkaClientSASLAvroProducerCSR.class.getSimpleName() + "<" + Date.from(now()) + ">");

            LOG.info("Sending some messages");
            List<Future<RecordMetadata>> futures = IntStream.range(0, 10000).mapToObj(i -> producer.send(
                            new ProducerRecord<>(topicName, key,
                                    buildApplicationLogEvent(key, "Value #" + i + " at " + Calendar.getInstance().getTime()))))
                    .collect(Collectors.toList());
            LOG.info("Produced some messages");

            // This is a primitive way of handling a collection of futures, just to illustrate the example.
            // Reading them inside the try-with-resources block, because if we let the producer close, the future won't be finished
            do {
                futures.removeIf(future -> {
                    if (!future.isDone()) {
                        return false;
                    }

                    try {
                        RecordMetadata producedMessage = future.get();
                        LOG.info("Produced message to topic {} partition {} offset {}", producedMessage.topic(),
                                producedMessage.partition(), producedMessage.offset());
                    } catch (InterruptedException | ExecutionException ignored) {
                    }
                    return true;
                });
                SleepUtil.sleep(Duration.ofMillis(100));
            } while (!futures.isEmpty());
            LOG.info("Closing the producer.");
        }
        LOG.info("Producer closed.");
    }

    private static void addSASLConfig(Map<String, Object> config) {
        config.put(SECURITY_PROTOCOL_CONFIG, SecurityProtocol.SASL_SSL.name);
        config.put(SASL_MECHANISM, ScramMechanism.SCRAM_SHA_512.mechanismName());
        config.put(SASL_JAAS_CONFIG, String.format(JAAS_TEMPLATE, appUsername, appPassword));
    }
}
