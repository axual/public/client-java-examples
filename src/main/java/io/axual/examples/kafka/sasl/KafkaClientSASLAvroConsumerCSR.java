//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.examples.kafka.sasl;

import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.security.auth.SecurityProtocol;
import org.apache.kafka.common.security.scram.internals.ScramMechanism;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static io.axual.examples.util.config.Configuration.APPLICATION_ID;
import static org.apache.kafka.clients.CommonClientConfigs.SECURITY_PROTOCOL_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.*;
import static org.apache.kafka.common.config.SaslConfigs.SASL_JAAS_CONFIG;
import static org.apache.kafka.common.config.SaslConfigs.SASL_MECHANISM;

import static io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG;

/**
 * Kafka Client Example (Consumer, Avro, Confluent Avro SerDes)
 */
public class KafkaClientSASLAvroConsumerCSR {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaClientSASLAvroConsumerCSR.class);
    private static final String JAAS_TEMPLATE = "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"%s\" password=\"%s\";";
    private static String appUsername;
    private static String appPassword;

    public static void main(String[] args) {
        Map<String, Object> config = new HashMap<>();
        // STEP 1: insert the name of the topic you created using Self-Service.
        // You can find the topic name by clicking the "i" icon on the topic detail page.
        String topicName = "insert-topic-name-here";

        // STEP 2: insert the credentials that you created using Self-service
        appUsername = "username"; // "Username" in the Self-Service user interface
        appPassword = "password"; // "Password" in the Self-Service user interface

        // Note: modify the BOOTSTRAP_SERVERS_CONFIG below if you are *not* connecting to the
        // shared Kafka environment provided by Axual
        // we are using Let's encrypt CA to sign the bootstrap server certificate
        config.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, "bootstrap-sharedkafka.byok.p.westeurope.azure.axual.cloud:9095");
        addSASLConfig(config);

        // Prepare consumer-specific configuration
        // Client.ID config
        //
        // An id string to pass to the server when making requests.
        // The purpose of this is to be able to track the source of requests beyond
        // just ip/port by allowing a logical application name to be included in server-side request logging.
        config.put(ConsumerConfig.CLIENT_ID_CONFIG, APPLICATION_ID);
        // Group.ID config
        //
        // STEP 3: insert the "Consumer group ID" of the application here
        config.put(ConsumerConfig.GROUP_ID_CONFIG, "insert-consumer-group-ID-here");

        // Deserialization config, we are using ApiCurio's AvroKafkaDeserializer here, both for the Key and the Value
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);

        // STEP 4: insert the "Confluent SR (Schema Registry) URL" here
        config.put(SCHEMA_REGISTRY_URL_CONFIG, "insert-schema-registry-URL-here");

        // Offset management
        config.put(AUTO_OFFSET_RESET_CONFIG, "earliest");
        config.put(ENABLE_AUTO_COMMIT_CONFIG, "true");

        // Batching config
        config.put(MAX_POLL_RECORDS_CONFIG, 3);

        // Instantiate the Kafka consumer, for which we prepared the configuration at the beginning.
        // Using try-with-resources since it implements the closeable interface.
        // In production, you wouldn't use it only once, and not in the main thread:
        //     you would instead have it poll in a loop on a separate thread, until a trigger stops it,
        //     allowing the try-with-resources block to close it afterwards.
        LOG.info("Instantiating consumer with properties: {}", config);
        try (Consumer<Application, ApplicationLogEvent> consumer = new KafkaConsumer<>(config)) {
            LOG.info("Consumer instantiated successfully.");

            consumer.subscribe(Collections.singleton(topicName));

            for (int i = 0; i < 5; i++) {
                LOG.info("Consuming poll #" + i);
                ConsumerRecords<Application, ApplicationLogEvent> consumerRecords = consumer.poll(Duration.ofSeconds(10L));

                // Add custom processing for consumed events. In this example we just log them.
                LOG.info("Consumed records: {}", StreamSupport.stream(consumerRecords.spliterator(), false).map(ConsumerRecord::toString)
                        .collect(Collectors.joining("\n\t", "\n\t", "\n")));

                LOG.info("Committing offsets");
                consumer.commitSync();
            }
            LOG.info("Closing consumer."); // The try-with-resources block will close it
        }
        LOG.info("Consumer closed.");
    }

    private static void addSASLConfig(Map<String, Object> config) {
        config.put(SECURITY_PROTOCOL_CONFIG, SecurityProtocol.SASL_SSL.name);
        config.put(SASL_MECHANISM, ScramMechanism.SCRAM_SHA_512.mechanismName());
        config.put(SASL_JAAS_CONFIG, String.format(JAAS_TEMPLATE, appUsername, appPassword));
    }
}
