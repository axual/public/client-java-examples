package io.axual.examples.kafka.sasl;

import static io.axual.examples.util.Util.getKafkaClientWithSASLConfig;
import static io.axual.examples.util.config.Configuration.APPLICATION_ID;

import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.axual.client.example.schema.ApplicationLogLevel;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;

import java.util.Collections;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Kafka Streams Example (Avro, Confluent Avro SerDes)
 */
public class KafkaStreamsSASLAvroToString {

    // Insert your source resolved topic e.g. {tenant}-{instance}-{environment}-{topic}
    public static final String SOURCE_TOPIC = "tenant-instance-environment-avro-applicationlog";
    // Insert your target resolved topic e.g. {tenant}-{instance}-{environment}-{topic}
    public static final String TARGET_TOPIC = "tenant-instance-environment-string-applicationlog";
    private static final Logger LOG = LoggerFactory.getLogger(KafkaStreamsSASLAvroToString.class);

    public static void main(String[] args) {
        Properties props = new Properties();
        props.putAll(getKafkaClientWithSASLConfig());

        // Client.ID config
        //
        // An id string to pass to the server when making requests.
        // The purpose of this is to be able to track the source of requests beyond
        // just ip/port by allowing a logical application name to be included in server-side request logging.
        props.put(StreamsConfig.CLIENT_ID_CONFIG, APPLICATION_ID);

        // Insert your resolved ID config e.g. {tenant}-{instance}-{environment}-{group}
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "tenant-instance-environment-stream_app");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

        props.put("schema.registry.url", "http://schema-registry:8080");
        // props.put("ssl.endpoint.identification.algorithm", "");

        props.put(StreamsConfig.CONNECTIONS_MAX_IDLE_MS_CONFIG, "180000");
        props.put(StreamsConfig.METADATA_MAX_AGE_CONFIG, "180000");

        // Configuration mapped to io.axual.client.config.DeliveryStrategy.AT_LEAST_ONCE
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ProducerConfig.ACKS_CONFIG, "-1");

        // Configuration mapped to io.axual.client.config.DeliveryStrategy.AT_MOST_ONCE
        // props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        // props.put(ProducerConfig.ACKS_CONFIG, "0");

        // Config when optimizeTopology = true
        props.put(StreamsConfig.TOPOLOGY_OPTIMIZATION_CONFIG, StreamsConfig.OPTIMIZE);

        // Config when optimizeTopology = false
        // props.put(StreamsConfig.TOPOLOGY_OPTIMIZATION_CONFIG, StreamsConfig.NO_OPTIMIZATION);

        StreamsBuilder builder = new StreamsBuilder();

        Map<String, String> serdeConfig = Collections.singletonMap("schema.registry.url", "http://schema-registry:8080");

        // Set up Avro serde for Application class
        final SpecificAvroSerde<Application> applicationSerde = new SpecificAvroSerde<>();
        applicationSerde.configure(serdeConfig, true);  // `true` signifies the serde is for serialization/deserialization of record key

        // Set up Avro serde for ApplicationLogEvent class
        final SpecificAvroSerde<ApplicationLogEvent> logEventSerde = new SpecificAvroSerde<>();
        logEventSerde.configure(serdeConfig, false);  // `false` signifies the serde is for serialization/deserialization of record value

        KStream<Application, ApplicationLogEvent> source = builder.stream(SOURCE_TOPIC, Consumed.with(applicationSerde, logEventSerde));
        source.peek((k, v) -> LOG.info("Key: {} Value: {}", k, v)).filter((k, v) -> v.getLevel().equals(ApplicationLogLevel.INFO))
                .map((k, v) -> new KeyValue<>(k.toString(), v.toString())).to(TARGET_TOPIC, Produced.with(Serdes.String(), Serdes.String()));

        KafkaStreams streams = new KafkaStreams(builder.build(), props);
        streams.setUncaughtExceptionHandler(exception -> {
            LOG.error("An exception occurred. Stopping application.", exception);
            return StreamsUncaughtExceptionHandler.StreamThreadExceptionResponse.SHUTDOWN_APPLICATION;
        });

        streams.start();

        // Add shutdown hook to respond to SIGTERM and gracefully close Kafka Streams
        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }

}
