//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.examples.kafka.mtls;

import static io.axual.examples.util.Util.getKafkaClientWithSSLConfig;
import static io.axual.examples.util.config.Configuration.APPLICATION_ID;
import static io.axual.examples.util.config.Configuration.GROUP_ID_RESOLVED;
import static io.axual.examples.util.config.Configuration.TOPIC_RESOLVED_NAME_STRING;
import static org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.MAX_POLL_RECORDS_CONFIG;

import java.time.Duration;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaClientSSLStringConsumer {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaClientSSLStringConsumer.class);

    public static void main(String[] args) {
        // Prepare the Kafka Client configuration. Check the Util classes for multiple SSL setup examples
        Map<String, Object> config = getKafkaClientWithSSLConfig();

        // Prepare consumer-specific configuration
        // Client.ID config
        //
        // An id string to pass to the server when making requests.
        // The purpose of this is to be able to track the source of requests beyond
        // just ip/port by allowing a logical application name to be included in server-side request logging.
        config.put(ConsumerConfig.CLIENT_ID_CONFIG, APPLICATION_ID);
        // Group.ID config
        //
        // A unique string that identifies the consumer group this consumer belongs to.
        config.put(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID_RESOLVED);
        // Deserialization config
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

        // Offset management
        config.put(AUTO_OFFSET_RESET_CONFIG, "earliest");
        config.put(ENABLE_AUTO_COMMIT_CONFIG, "false");

        // Batching config
        config.put(MAX_POLL_RECORDS_CONFIG, 3);

        // Instantiate the Kafka consumer, for which we prepared the configuration at the beginning.
        // Using try-with-resources since it implements the closeable interface.
        // In production, you wouldn't use it only once, and not in the main thread:
        //     you would instead have it poll in a loop on a separate thread, until a trigger stops it,
        //     allowing the try-with-resources block to close it afterwards.
        LOG.info("Instantiating consumer with properties: {}", config);
        try (Consumer<String, String> consumer = new KafkaConsumer<>(config)) {
            LOG.info("Consumer instantiated successfully.");

            consumer.subscribe(Collections.singleton(TOPIC_RESOLVED_NAME_STRING));

            for (int i = 0; i < 5; i++) {
                LOG.info("Consuming poll #" + i);
                ConsumerRecords<String, String> consumerRecords = consumer.poll(Duration.ofSeconds(2));

                // Add custom processing for consumed events. In this example we just log them.
                LOG.info("Consumed records: {}", StreamSupport.stream(consumerRecords.spliterator(), false).map(ConsumerRecord::toString)
                    .collect(Collectors.joining("\n\t", "\n\t", "\n")));

                LOG.info("Committing offsets");
                consumer.commitSync();
            }
            LOG.info("Closing consumer."); // The try-with-resources block will close it
        }
        LOG.info("Consumer closed.");
    }
}
