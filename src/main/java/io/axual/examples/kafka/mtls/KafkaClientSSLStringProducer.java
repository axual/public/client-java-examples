//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.examples.kafka.mtls;

import static io.axual.examples.util.Util.getKafkaClientWithSSLConfig;
import static io.axual.examples.util.config.Configuration.APPLICATION_ID;
import static io.axual.examples.util.config.Configuration.TOPIC_RESOLVED_NAME_STRING;
import static java.time.Instant.now;

import io.axual.common.tools.SleepUtil;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaClientSSLStringProducer {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaClientSSLStringProducer.class);

    public static void main(String[] args) {
        // Prepare the Kafka Client configuration. Check the Util classes for multiple SSL setup examples
        Map<String, Object> config = getKafkaClientWithSSLConfig();

        // Prepare producer-specific configuration
        // Client.ID config
        //
        // An id string to pass to the server when making requests.
        // The purpose of this is to be able to track the source of requests beyond
        // just ip/port by allowing a logical application name to be included in server-side request logging.
        config.put(ProducerConfig.CLIENT_ID_CONFIG, APPLICATION_ID);

        // Serialization config
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

        // "Delivery & Ordering strategy"
        config.put(ProducerConfig.ACKS_CONFIG, "-1");
        config.put(ProducerConfig.RETRIES_CONFIG, "2");

        // Batching config
        config.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "1");

        // Timeout values
        config.put(ProducerConfig.RETRY_BACKOFF_MS_CONFIG, "1000"); // Only relevant when RETRIES_CONFIG > 0
        config.put(ProducerConfig.RECONNECT_BACKOFF_MAX_MS_CONFIG, "1000");

        // Instantiate the Kafka producer, for which we prepared the configuration at the beginning.
        // Using try-with-resources since it implements the closeable interface.
        // In production, you wouldn't use try with resources:
        //     you would instead instantiate it once and REUSE it throughout the project's lifetime,
        //     finally closing it during application shutdown.
        LOG.info("Instantiating producer with properties: {}", config);
        try (Producer<String, String> producer = new KafkaProducer<>(config)) {
            LOG.info("Producer instantiated successfully.");

            // This string will be included in every message for aspect only
            String key = KafkaClientSSLStringProducer.class.getSimpleName() + "<" + Date.from(now()) + ">";

            LOG.info("Sending some messages.");
            List<Future<RecordMetadata>> futures = IntStream.range(0, 10).mapToObj(i -> producer.send(
                    new ProducerRecord<>(TOPIC_RESOLVED_NAME_STRING, key, "Value #" + i + " at " + Calendar.getInstance().getTime())))
                .collect(Collectors.toList());
            LOG.info("Produced some messages.");

            // This is a primitive way of handling a collection of futures, just to illustrate the example.
            // Reading them inside the try-with-resources block, because if we let the producer close, the future won't be finished
            do {
                futures.removeIf(future -> {
                    if (!future.isDone()) {
                        return false;
                    }

                    try {
                        RecordMetadata producedMessage = future.get();
                        LOG.info("Produced message to topic {} partition {} offset {}", producedMessage.topic(),
                            producedMessage.partition(), producedMessage.offset());
                    } catch (InterruptedException | ExecutionException ignored) {
                    }
                    return true;
                });
                SleepUtil.sleep(Duration.ofMillis(100));
            } while (!futures.isEmpty());
            LOG.info("Closing the producer.");
        }
        LOG.info("Producer closed.");
    }
}
