//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.examples.axual.streamproxy;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Map;

import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.axual.client.example.schema.ApplicationLogLevel;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.common.tools.SleepUtil;
import io.axual.serde.avro.SpecificAvroSerde;
import io.axual.streams.proxy.axual.AxualStreams;
import io.axual.streams.proxy.axual.AxualStreamsConfig;
import io.axual.streams.proxy.generic.factory.TopologyFactory;
import io.axual.streams.proxy.generic.factory.UncaughtExceptionHandlerFactory;
import io.axual.streams.proxy.wrapped.WrappedStreamsConfig;

import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.RESOLVING_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.SWITCHING_PROXY_ID;
import static io.axual.examples.util.config.Configuration.TOPIC_NAME_AVRO;
import static io.axual.examples.util.config.Configuration.TOPIC_NAME_STRING;
import static io.axual.examples.util.Util.getAxualClientProxyConfig;

@Deprecated(since="6.0.7")
public class AxualStreamProxySSLAvroToStringApp {

    private static final Logger LOG = LoggerFactory.getLogger(AxualStreamProxySSLAvroToStringApp.class);

    public static void main(String[] args) {
        // Prepare the Axual Client configuration. Check the Util classes for multiple SSL setup examples
        Map<String, Object> config = getAxualClientProxyConfig();

        // Prepare streaming-specific configuration
        // Serialization config
        config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, SpecificAvroSerde.class);
        config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, SpecificAvroSerde.class);

        config.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, 4);
        config.put(WrappedStreamsConfig.UNCAUGHT_EXCEPTION_HANDLER_FACTORY_CONFIG,
                (UncaughtExceptionHandlerFactory) streams -> throwable -> {
                    LOG.error("An exception occurred. Stopping application.", throwable);
                    return StreamsUncaughtExceptionHandler.StreamThreadExceptionResponse.SHUTDOWN_APPLICATION;
                });

        // "Delivery & Ordering strategy"
        config.put(ProducerConfig.ACKS_CONFIG, "-1");
        config.put(ProducerConfig.RETRIES_CONFIG, "2");

        // The proxy chain
        config.put(AxualStreamsConfig.CHAIN_CONFIG, ProxyChain.newBuilder()
                .append(SWITCHING_PROXY_ID)
                .append(RESOLVING_PROXY_ID)
//                .append(LINEAGE_PROXY_ID)
                .build());


        LOG.info("Creating Topology...");
        /*
         * Steps:
         * 1- Read stream from the AVRO kafka-topic.
         * 2- Filter the records based on logging level.
         * 3- Transform the record (key and value) from avro to string format.
         * 4- Write to the string kafka-topic.
         */
        final TopologyFactory topologyFactory = builder -> {
            KStream<Application, ApplicationLogEvent> sourceStream = builder.stream(TOPIC_NAME_AVRO);
            sourceStream
                    .peek((k, v) -> LOG.info("Key: {} Value: {}", k, v))
                    .filter((k, v) -> v.getLevel().equals(ApplicationLogLevel.INFO))
                    .map((k, v) -> new KeyValue<>(k.toString(), v.toString()))
                    .to(TOPIC_NAME_STRING, Produced.with(Serdes.String(), Serdes.String()));
            return builder.build();
        };

        config.put(WrappedStreamsConfig.TOPOLOGY_FACTORY_CONFIG, topologyFactory);

        LOG.info("Creating and starting AxualStreams...");
        try (AxualStreams streamRunner = new AxualStreams(config)) {
            streamRunner.start();
            SleepUtil.sleep(Duration.ofSeconds(10));
            streamRunner.stop();
            // The try with resources will close the streamsClient.
            // In production, you would close the streamsClient during application shutdown
            LOG.info("Closing the Axual stream runner.");
        }
        LOG.info("Closed the Axual stream runner.");
    }
}
