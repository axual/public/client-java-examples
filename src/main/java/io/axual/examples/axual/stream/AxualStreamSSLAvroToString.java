//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.examples.axual.stream;

import io.axual.client.config.DeliveryStrategy;
import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.axual.client.example.schema.ApplicationLogLevel;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.common.config.ClientConfig;
import io.axual.common.tools.SleepUtil;
import io.axual.examples.util.Util;
import io.axual.serde.avro.SpecificAvroSerde;
import io.axual.streams.AxualStreamsClient;
import io.axual.streams.config.StreamRunnerConfig;
import io.axual.streams.proxy.generic.factory.TopologyFactory;
import io.axual.streams.streams.StreamRunner;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.RESOLVING_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.SWITCHING_PROXY_ID;
import static io.axual.examples.util.config.Configuration.TOPIC_NAME_AVRO;
import static io.axual.examples.util.config.Configuration.TOPIC_NAME_STRING;

@Deprecated(since="6.0.7")
public class AxualStreamSSLAvroToString {

    private static final Logger LOG = LoggerFactory.getLogger(AxualStreamSSLAvroToString.class);

    public static void main(String[] args) {
        // Prepare the Axual Client configuration. Check the Util classes for multiple SSL setup examples
        ClientConfig config = Util.getClientConfig();

        LOG.info("Creating Topology.");
        /*
         * Steps:
         * 1- Read stream from the AVRO kafka-topic.
         * 2- Filter the records based on logging level.
         * 3- Transform the record (key and value) from avro to string format.
         * 4- Write to the string kafka-topic.
         * */
        final TopologyFactory topology = builder -> {
            KStream<Application, ApplicationLogEvent> sourceStream = builder.stream(TOPIC_NAME_AVRO);
            sourceStream
                    .peek((k, v) -> LOG.info("Key: {} Value: {}", k, v))
                    .filter((k, v) -> v.getLevel().equals(ApplicationLogLevel.INFO))
                    .map((k, v) -> new KeyValue<>(k.toString(), v.toString()))
                    .to(TOPIC_NAME_STRING, Produced.with(Serdes.String(), Serdes.String()));
            return builder.build();
        };

        LOG.info("Creating StreamRunnerConfig.");
        // Prepare streamRunner
        final StreamRunnerConfig streamRunnerConfig = StreamRunnerConfig.builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setDefaultKeySerde(SpecificAvroSerde.class)
                .setDefaultValueSerde(SpecificAvroSerde.class)
                .setUncaughtExceptionHandler(streams -> throwable -> {
                    LOG.error("An exception occurred. Stopping application.", throwable);
                    return StreamsUncaughtExceptionHandler.StreamThreadExceptionResponse.SHUTDOWN_APPLICATION;
                })
                .setProxyChain(ProxyChain.newBuilder()
                        .append(SWITCHING_PROXY_ID)
                        .append(RESOLVING_PROXY_ID)
//                        .append(LINEAGE_PROXY_ID)
                        .build())
                .setTopologyFactory(topology)
                .build();

        LOG.info("Creating AxualStreamsClient and StreamRunner.");
        try(AxualStreamsClient streamsClient = new AxualStreamsClient(config)) {
            StreamRunner streamRunner = streamsClient.buildStreamRunner(streamRunnerConfig);
            streamRunner.start();
            SleepUtil.sleep(Duration.ofSeconds(10));
            streamRunner.stop();
            // The try with resources will close the streamsClient.
            // In production, you would close the streamsClient during application shutdown
            LOG.info("Closing the AxualStreamsClient. It will also close the streamRunner.");
        }
        LOG.info("Closed the AxualStreamsClient.");
    }
}
