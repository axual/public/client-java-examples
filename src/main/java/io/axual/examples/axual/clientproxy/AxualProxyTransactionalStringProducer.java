//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.examples.axual.clientproxy;

import io.axual.client.proxy.axual.producer.AxualProducer;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.client.proxy.switching.producer.TransactionSwitchedException;
import io.axual.common.tools.SleepUtil;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.errors.AuthorizationException;
import org.apache.kafka.common.errors.OutOfOrderSequenceException;
import org.apache.kafka.common.errors.ProducerFencedException;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static io.axual.client.proxy.axual.producer.AxualProducerConfig.CHAIN_CONFIG;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.RESOLVING_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.SWITCHING_PROXY_ID;
import static io.axual.examples.util.config.Configuration.TOPIC_NAME_STRING;
import static io.axual.examples.util.Util.getAxualClientProxyConfig;
import static java.time.Instant.now;

@Deprecated(since="6.0.7")
public class AxualProxyTransactionalStringProducer {

    private static final Logger LOG = LoggerFactory.getLogger(AxualProxyTransactionalStringProducer.class);

    public static void main(String[] args) {
        // Prepare the Axual Client-Proxy configuration. Check the Util classes for multiple SSL setup examples
        Map<String, Object> config = getAxualClientProxyConfig();

        // Prepare producer-specific configuration
        // Serialization config
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

        // "Delivery & Ordering strategy"
        config.put(ProducerConfig.ACKS_CONFIG, "-1");
        config.put(ProducerConfig.RETRIES_CONFIG, "2");

        // Batching config
        config.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "1");

        // Timeout values
        config.put(ProducerConfig.RETRY_BACKOFF_MS_CONFIG, "1000"); // Only relevant when RETRIES_CONFIG > 0
        config.put(ProducerConfig.RECONNECT_BACKOFF_MAX_MS_CONFIG, "1000");

        // The proxy chain
        config.put(CHAIN_CONFIG, ProxyChain.newBuilder()
                .append(SWITCHING_PROXY_ID)
                .append(RESOLVING_PROXY_ID)
//                .append(LINEAGE_PROXY_ID)
                .build());

        // Transactional functionality: Note that "Retries" config is larger than zero and ACKs are required.
        config.put(ProducerConfig.TRANSACTIONAL_ID_CONFIG, "example");
        config.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");

        // Instantiate the Axual-Client-Proxy producer, for which we prepared the configuration at the beginning.
        // Using try-with-resources since it implements the closeable interface.
        // In production, you wouldn't use try with resources:
        //     you would instead instantiate it once and REUSE it throughout the project's lifetime,
        //     finally closing it during application shutdown.
        LOG.info("Instantiating producer with properties: {}", config);
        try (Producer<String, String> producer = new AxualProducer<>(config)) {
            LOG.info("Producer instantiated successfully.");

            // initTransactions() is called only at the beginning of the producer's lifetime
            producer.initTransactions();

            // This string will be included in every message for aspect only
            String key = AxualProxyTransactionalStringProducer.class.getSimpleName() + "<" + Date.from(now()) + ">";

            // Send 10 messages over 2 transactions
            // Transactional APIs are blocking and will throw exceptions on failure
            // In production, the transactions should be wrapped in their own try-catch blocks.
            // see https://kafka.apache.org/11/javadoc/org/apache/kafka/clients/producer/KafkaProducer.html
            LOG.info("Sending some messages.");

            producer.beginTransaction();
            List<Future<RecordMetadata>> futures = IntStream.range(0, 5)
                    .mapToObj(i -> producer.send(
                            new ProducerRecord<>(
                                    TOPIC_NAME_STRING,
                                    key,
                                    "Value #" + i + " at " + Calendar.getInstance().getTime())
                    ))
                    .collect(Collectors.toList());
            producer.commitTransaction();
            LOG.info("Produced some messages in a transaction.");

            // Begin a second transaction
            producer.beginTransaction();
            futures.addAll(IntStream.range(5, 10)
                    .mapToObj(i -> producer.send(
                            new ProducerRecord<>(
                                    TOPIC_NAME_STRING,
                                    key,
                                    "Value #" + i + " at " + Calendar.getInstance().getTime())
                    ))
                    .collect(Collectors.toList()));
            producer.commitTransaction();
            LOG.info("Produced some messages in a second transaction.");

            // This is a primitive way of handling a collection of futures, just to illustrate the example.
            // Reading them inside the try-with-resources block, because if we let the producer close, the future won't be finished
            do {
                futures.removeIf(future -> {
                    if (!future.isDone()) {
                        return false;
                    }

                    try {
                        RecordMetadata producedMessage = future.get();
                        LOG.info("Produced message to topic {} partition {} offset {}", producedMessage.topic(), producedMessage.partition(), producedMessage.offset());
                    } catch (InterruptedException | ExecutionException ignored) {}
                    return true;
                });
                SleepUtil.sleep(Duration.ofMillis(100));
            } while (!futures.isEmpty());
            LOG.info("Closing the producer.");
        } catch (ProducerFencedException | OutOfOrderSequenceException | AuthorizationException e) {
            LOG.error("Non-recoverable error during transactional produce", e);
            // These are non-recoverable, we can only close the producer and start over
            // (in this example, closing is done by the try-with-resources block
            throw e;
        } catch (TransactionSwitchedException e) {
            LOG.error("The switching-producer switched to a different cluster "
                    + "during an ongoing transactional produce sequence. "
                    + "Transactional context has been lost.", e);
            // In this case, call beginTransaction() again
            // and resend all the messages corresponding to the failed transaction.
        }
        LOG.info("Producer closed.");
    }
}
