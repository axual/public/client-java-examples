//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.examples.axual.clientproxy;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.axual.client.proxy.axual.producer.AxualProducer;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.common.tools.SleepUtil;

import static io.axual.client.proxy.axual.producer.AxualProducerConfig.CHAIN_CONFIG;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.RESOLVING_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.SWITCHING_PROXY_ID;
import static io.axual.examples.util.AvroUtil.buildApplication;
import static io.axual.examples.util.AvroUtil.buildApplicationLogEvent;
import static io.axual.examples.util.config.Configuration.TOPIC_NAME_AVRO;
import static io.axual.examples.util.Util.getAxualClientProxyConfig;
import static java.time.Instant.now;

@Deprecated(since="6.0.7")
public class AxualProxyAvroProducer {

    private static final Logger LOG = LoggerFactory.getLogger(AxualProxyAvroProducer.class);

    public static void main(String[] args) {
        // Prepare the Axual Client configuration. Check the Util classes for multiple SSL setup examples
        Map<String, Object> config = getAxualClientProxyConfig();

        // Prepare producer-specific configuration
        // Serialization config
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "io.axual.serde.avro.SpecificAvroSerializer");
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "io.axual.serde.avro.SpecificAvroSerializer");

        // "Delivery & Ordering strategy"
        config.put(ProducerConfig.ACKS_CONFIG, "-1");
        config.put(ProducerConfig.RETRIES_CONFIG, "2");

        // Batching config
        config.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "1");

        // Timeout values
        config.put(ProducerConfig.RETRY_BACKOFF_MS_CONFIG, "1000"); // Only relevant when RETRIES_CONFIG > 0
        config.put(ProducerConfig.RECONNECT_BACKOFF_MAX_MS_CONFIG, "1000");

        // The proxy chain
        config.put(CHAIN_CONFIG, ProxyChain.newBuilder()
                .append(SWITCHING_PROXY_ID)
                .append(RESOLVING_PROXY_ID)
//                .append(LINEAGE_PROXY_ID)
                .build());

        // Instantiate the Axual-Client-Proxy producer, for which we prepared the configuration at the beginning.
        // Using try-with-resources since it implements the closeable interface.
        // In production, you wouldn't use try with resources:
        //     you would instead instantiate it once and REUSE it throughout the project's lifetime,
        //     finally closing it during application shutdown.
        LOG.info("Instantiating producer with properties: {}", config);
        try (Producer<Application, ApplicationLogEvent> producer = new AxualProducer<>(config)) {
            LOG.info("Producer instantiated successfully.");

            // This string will be included in every message for aspect only
            Application key = buildApplication(
                    AxualProxyAvroProducer.class.getSimpleName() + "<" + Date.from(now()) +">"
            );

            LOG.info("Sending some messages.");
            List<Future<RecordMetadata>> futures = IntStream.range(0, 10)
                    .mapToObj(i -> producer.send(
                            new ProducerRecord<>(
                                    TOPIC_NAME_AVRO,
                                    key,
                                    buildApplicationLogEvent(key, "Value #" + i + " at " + Calendar.getInstance().getTime()))
                    ))
                    .collect(Collectors.toList());
            LOG.info("Produced some messages.");

            // This is a primitive way of handling a collection of futures, just to illustrate the example.
            // Reading them inside the try-with-resources block, because if we let the producer close, the future won't be finished
            do {
                futures.removeIf(future -> {
                    if (!future.isDone()) {
                        return false;
                    }

                    try {
                        RecordMetadata producedMessage = future.get();
                        LOG.info("Produced message to topic {} partition {} offset {}", producedMessage.topic(), producedMessage.partition(), producedMessage.offset());
                    } catch (InterruptedException | ExecutionException ignored) {}
                    return true;
                });
                SleepUtil.sleep(Duration.ofMillis(100));
            } while (!futures.isEmpty());
            LOG.info("Closing the producer.");
        }
        LOG.info("Producer closed.");
    }
}
