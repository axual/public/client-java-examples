//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.


package io.axual.examples.axual.client;

import io.axual.client.AxualClient;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.OrderingStrategy;
import io.axual.client.config.SpecificAvroProducerConfig;
import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.axual.client.producer.ProducedMessage;
import io.axual.client.producer.Producer;
import io.axual.client.producer.ProducerMessage;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.common.config.ClientConfig;
import io.axual.common.tools.SleepUtil;
import io.axual.examples.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.RESOLVING_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.SWITCHING_PROXY_ID;
import static io.axual.examples.util.AvroUtil.buildApplication;
import static io.axual.examples.util.AvroUtil.buildApplicationLogEvent;
import static io.axual.examples.util.config.Configuration.TOPIC_NAME_AVRO;
import static java.time.Instant.now;

@Deprecated(since="6.0.7")
public class AxualClientAvroProducer {

    private static final Logger LOG = LoggerFactory.getLogger(AxualClientAvroProducer.class);

    public static void main(String[] args) {
        // Prepare the Axual Client configuration. Check the Util classes for multiple SSL setup examples
        ClientConfig config = Util.getClientConfig();

        // Prepare the consumer configuration
        SpecificAvroProducerConfig<Application, ApplicationLogEvent> producerConfig =
                SpecificAvroProducerConfig.<Application, ApplicationLogEvent>builder()
                        .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                        .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
                        .setBatchSize(1)
                        .setMessageBufferWaitTimeout(100)
                        .setProxyChain(ProxyChain.newBuilder()
                                .append(SWITCHING_PROXY_ID)
                                .append(RESOLVING_PROXY_ID)
//                                .append(LINEAGE_PROXY_ID)
                                .build())
                        .build();


        // Instantiate the Axual Client and the Axual-Producer, for which we prepared the configuration at the beginning.
        // Using try-with-resources since they both implement the closeable interface.
        // In production, you wouldn't use try with resources:
        //     you would instead instantiate them once and REUSE them throughout the project's lifetime,
        //     finally closing them during application shutdown.
        LOG.info("Instantiating axualClient with properties: {}", config);
        LOG.info("Instantiating producer with properties: {}", producerConfig);
        try (AxualClient axualClient = new AxualClient(config);
             final Producer<Application, ApplicationLogEvent> producer = axualClient.buildProducer(producerConfig) ) {
            LOG.info("Producer instantiated successfully.");

            // This string will be included in every message for aspect only
            Application key = buildApplication(
                    AxualClientAvroProducer.class.getSimpleName() + "<" + Date.from(now()) +">"
            );

            LOG.info("Sending some messages");
            List<Future<ProducedMessage<Application, ApplicationLogEvent>>> futures = IntStream.range(0, 10)
                    .mapToObj(i -> producer.produce(ProducerMessage.<Application, ApplicationLogEvent>newBuilder()
                            .setStream(TOPIC_NAME_AVRO)
                            .setKey(key)
                            .setValue(buildApplicationLogEvent(key, "Value #" + i + " at " + Calendar.getInstance().getTime()))
                            .build()))
                    .collect(Collectors.toList());
            LOG.info("Produced some messages");

            // This is a primitive way of handling a collection of futures, just to illustrate the example.
            // Reading them inside the try-with-resources block, because if we let the producer close, the future won't be finished
            do {
                futures.removeIf(future -> {
                    if (!future.isDone()) {
                        return false;
                    }

                    try {
                        ProducedMessage<Application, ApplicationLogEvent> producedMessage = future.get();
                        LOG.info("Produced message to topic {} partition {} offset {}", producedMessage.getStream(), producedMessage.getPartition(), producedMessage.getOffset());
                    } catch (InterruptedException | ExecutionException ignored) {}
                    return true;
                });
                SleepUtil.sleep(Duration.ofMillis(100));
            } while (!futures.isEmpty());
            LOG.info("Closing the producer.");
        }
        LOG.info("Producer closed.");
    }
}
