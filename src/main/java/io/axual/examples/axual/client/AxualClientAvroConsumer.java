//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.examples.axual.client;

import io.axual.client.AxualClient;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.SpecificAvroConsumerConfig;
import io.axual.client.consumer.Consumer;
import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.axual.client.exception.ConsumeFailedException;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.common.config.ClientConfig;
import io.axual.common.tools.SleepUtil;
import io.axual.examples.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.RESOLVING_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.SWITCHING_PROXY_ID;
import static io.axual.examples.util.config.Configuration.TOPIC_NAME_AVRO;

@Deprecated(since="6.0.7")
public class AxualClientAvroConsumer {

    private static final Logger LOG = LoggerFactory.getLogger(AxualClientAvroConsumer.class);

    public static void main(String[] args) {
        // Prepare the Axual Client configuration. Check the Util classes for multiple SSL setup examples
        ClientConfig config = Util.getClientConfig();

        // Prepare the consumer configuration
        SpecificAvroConsumerConfig<Application, ApplicationLogEvent> consumerConfig =
                SpecificAvroConsumerConfig.<Application, ApplicationLogEvent>builder()
                        .setStream(TOPIC_NAME_AVRO)
                        .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                        .setMaximumPollSize(1)
                        .setProxyChain(ProxyChain.newBuilder()
                                .append(SWITCHING_PROXY_ID)
                                .append(RESOLVING_PROXY_ID)
//                                .append(LINEAGE_PROXY_ID)
                                .build())
                        .build();

        // Instantiate the Axual Client and the Axual Consumer
        // Using try-with-resources since they both implement the closeable interface.
        // In production, you wouldn't use try with resources:
        //     you would instead instantiate them once and REUSE them throughout the project's lifetime,
        //     finally closing them during application shutdown.
        try (
                AxualClient axualClient = new AxualClient(config);
                Consumer<Application, ApplicationLogEvent> consumer = axualClient.buildConsumer(
                        consumerConfig,
                        // Add custom processing for consumed events. In this example we just log them.
                        consumerMessage -> LOG.info("Consumed message:\n\tKey: {}\n\tValue: {}\n", consumerMessage.getKey(), consumerMessage.getValue())
                );
        ) {
            LOG.info("Starting consumer for 10 seconds.");
            consumer.startConsuming();
            SleepUtil.sleep(Duration.ofSeconds(30));
            LOG.info("Stopping consumer.");
            ConsumeFailedException cfe = consumer.stopConsuming();
            if (cfe != null) {
                throw cfe;
            }
            LOG.info("Closing consumer."); // The try-with-resources block will close it
        }
        LOG.info("Consumer closed.");
    }
}
